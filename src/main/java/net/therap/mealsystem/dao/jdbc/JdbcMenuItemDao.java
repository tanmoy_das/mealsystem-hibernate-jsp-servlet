package net.therap.mealsystem.dao.jdbc;

import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.mapper.MenuItemMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class JdbcMenuItemDao {

    private Connection connection;
    private MenuItemMapper menuItemMapper;

    public JdbcMenuItemDao(Connection connection, MenuItemMapper menuItemMapper) {
        this.connection = connection;
        this.menuItemMapper = menuItemMapper;
    }

    public Optional<MenuItem> get(int id) throws SQLException {
        String sqlSelect = "SELECT * FROM menuItems WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        return menuItemMapper.map(resultSet);
    }

    public List<MenuItem> getAll() throws SQLException {
        List<MenuItem> menuItems = new ArrayList<>();

        String sqlSelect = "SELECT * FROM menuItems";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (!resultSet.isAfterLast()) {
            Optional<MenuItem> optionalMenuItem = menuItemMapper.map(resultSet);
            optionalMenuItem.ifPresent(menuItems::add);
        }

        return menuItems;
    }

    public Optional<MenuItem> save(MealTime mealTime, MenuItem menuItem) throws SQLException {
        String sqlInsert = "INSERT INTO menuItems (mealTimeId, foodId, maxAmount) VALUES (?, ?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
        preparedStatement.setInt(1, mealTime.getId());
        preparedStatement.setInt(2, menuItem.getFood().getId());
        preparedStatement.setString(3, menuItem.getMaximumAmount());
        preparedStatement.executeUpdate();


        return getMenuItemByData(mealTime.getId(), menuItem.getFood().getId());
    }

    public Optional<MenuItem> getMenuItemByData(int mealTimeId, int foodId) throws SQLException {
        String sqlSelect = "SELECT * FROM menuItems WHERE mealTimeId = ? and foodId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        preparedStatement.setInt(1, mealTimeId);
        preparedStatement.setInt(2, foodId);


        ResultSet resultSet = preparedStatement.executeQuery();
        return menuItemMapper.map(resultSet);
    }


    public void update(MealTime mealTime, MenuItem menuItem) throws SQLException {
        String sqlUpdate = "UPDATE menuItems SET mealTimeId = ?, foodId = ?, maxAmount = ? WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
        preparedStatement.setInt(1, mealTime.getId());
        preparedStatement.setInt(2, menuItem.getFood().getId());
        preparedStatement.setString(3, menuItem.getMaximumAmount());
        preparedStatement.setInt(4, menuItem.getId());
        preparedStatement.executeUpdate();
    }

    public void delete(MenuItem menuItem) throws SQLException {
        String sqlDelete = "DELETE FROM menuItems WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
        preparedStatement.setInt(1, menuItem.getId());
        preparedStatement.executeUpdate();
    }
}
