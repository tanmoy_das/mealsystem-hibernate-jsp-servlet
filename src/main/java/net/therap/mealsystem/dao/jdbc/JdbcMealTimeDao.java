package net.therap.mealsystem.dao.jdbc;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.mapper.MealTimeMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class JdbcMealTimeDao implements MealTimeDao {

    private Connection connection;
    private MealTimeMapper mealTimeMapper;

    public JdbcMealTimeDao(Connection connection, MealTimeMapper mealTimeMapper) throws SQLException {
        this.connection = connection;
        this.mealTimeMapper = mealTimeMapper;
    }

    @Override
    public Optional<MealTime> find(int id) throws SQLException {
        String sqlSelect = "SELECT * FROM mealTimes WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        return mealTimeMapper.map(resultSet);
    }

    @Override
    public List<MealTime> findAll() throws SQLException {
        List<MealTime> mealTimes = new ArrayList<>();
        String sqlSelect = "SELECT * FROM mealTimes";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (!resultSet.isAfterLast()) {
            Optional<MealTime> optionalMealTime = mealTimeMapper.map(resultSet);
            optionalMealTime.ifPresent(mealTimes::add);
        }

        return mealTimes;
    }

    @Override
    public Optional<MealTime> save(MealTime mealTime) throws SQLException {
        String sqlInsert = "INSERT INTO mealTimes (day, time) VALUES (?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
        preparedStatement.setString(1, mealTime.getDay().toString());
        preparedStatement.setString(2, mealTime.getTime());
        preparedStatement.executeUpdate();

        return findByDayTime(mealTime.getDay(), mealTime.getTime());
    }

    @Override
    public void saveOrUpdate(MealTime mealTime) throws SQLException {
        Optional<MealTime> optionalMealTime = findByDayTime(mealTime.getDay(), mealTime.getTime());

        if (optionalMealTime.isPresent()) {
            update(mealTime);
        } else {
            save(mealTime);
        }
    }

    @Override
    public Optional<MealTime> findByDayTime(Day day, String time) throws SQLException {
        String sqlSelect = "SELECT * FROM mealTimes WHERE day = ? and time = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        preparedStatement.setString(1, day.getName());
        preparedStatement.setString(2, time);


        ResultSet resultSet = preparedStatement.executeQuery();
        return mealTimeMapper.map(resultSet);
    }

    @Override
    public List<MealTime> findByDay(Day day) throws SQLException {
        String sqlSelect = "SELECT * FROM mealTimes WHERE day = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);

        preparedStatement.setString(1, day.getName());
        ResultSet resultSet = preparedStatement.executeQuery();

        List<MealTime> mealTimes = new ArrayList<>();
        while (!resultSet.isAfterLast()) {
            Optional<MealTime> optionalMealTime = mealTimeMapper.map(resultSet);
            optionalMealTime.ifPresent(mealTimes::add);
        }

        return mealTimes;
    }


    @Override
    public void update(MealTime mealTime) throws SQLException {
        String sqlUpdate = "UPDATE mealTimes SET day = ?, time = ? WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
        preparedStatement.setString(1, mealTime.getDay().getName());
        preparedStatement.setString(2, mealTime.getTime());
        preparedStatement.setInt(3, mealTime.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(MealTime mealTime) throws SQLException {
        String sqlDelete = "DELETE FROM food WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
        preparedStatement.setInt(1, mealTime.getId());
        preparedStatement.executeUpdate();
    }
}
