package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MealTimeDao {
    Optional<MealTime> find(int id) throws SQLException;

    List<MealTime> findAll() throws SQLException;

    Optional<MealTime> save(MealTime mealTime) throws SQLException;

    void saveOrUpdate(MealTime mealTime) throws SQLException;

    Optional<MealTime> findByDayTime(Day day, String time) throws SQLException;

    void update(MealTime mealTime) throws SQLException;

    void delete(MealTime mealTime) throws SQLException;

    List<MealTime> findByDay(Day day) throws SQLException;
}
