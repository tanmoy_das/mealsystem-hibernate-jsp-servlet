package net.therap.mealsystem.mapper;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.domain.MenuItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class MenuItemMapper {

    private FoodDao foodDao;

    public MenuItemMapper(FoodDao foodDao) {
        this.foodDao = foodDao;
    }

    public Optional<MenuItem> map(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            int id = resultSet.getInt("id");

            int foodId = resultSet.getInt("foodId");
            String maxAmount = resultSet.getString("maxAmount");

            Optional<Food> optionalFood = foodDao.find(foodId);
            return optionalFood.map(food -> new MenuItem(id, food, maxAmount));

        } else {
            return Optional.empty();
        }
    }
}
