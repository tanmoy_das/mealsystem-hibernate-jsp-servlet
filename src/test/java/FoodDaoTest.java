import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.jdbc.JdbcFoodDao;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.mapper.FoodMapper;
import net.therap.mealsystem.util.MySqlConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class FoodDaoTest {
    public static void main(String[] args) {

        try (Connection connection = MySqlConnectionManager.createConnection()) {

            if (connection != null) {
                System.out.println("Connection successful");

                FoodDao foodDao = new JdbcFoodDao(connection, new FoodMapper());

                List<Food> foods = new ArrayList<>();

                foods.add(new Food("bugichugi"));
                foods.add(new Food("bugichugi2"));
                foods.add(new Food("bugichugi3"));
                foods.add(new Food("bugichugi4"));

                for (Food food : foods) {
                    Optional<Food> savedFood = foodDao.save(food);
                    savedFood.ifPresent(value -> food.setId(value.getId()));
                }

                Food firstFood = foods.get(0);
                firstFood.setName(firstFood.getName() + " fruit");

                foodDao.update(firstFood);

                for (int i = 1; i < foods.size(); i++) {
                    Food food = foods.get(i);
                    food.setName(food.getName() + " flower");
                }

                for (Food food : foods) {
                    if (food.getId() % 2 == 0) {
                        foodDao.delete(food);
                    }
                }

                foods = foodDao.findAll();

                assert (foods.get(0).getId() == foodDao.find(foods.get(0).getId()).get().getId());
                System.out.println(foods.get(0));
            } else {
                System.out.println("Failed to make connection!");
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s%n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
