package net.therap.mealsystem.mapper;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.jdbc.JdbcMenuItemDao;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class MenuMapper {

    private MealTimeDao mealTimeDao;
    private JdbcMenuItemDao menuItemDao;

    public MenuMapper(MealTimeDao mealTimeDao, JdbcMenuItemDao menuItemDao) {
        this.mealTimeDao = mealTimeDao;
        this.menuItemDao = menuItemDao;
    }

    public Optional<Menu> map(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            int menuItemId = resultSet.getInt("id");
            int mealTimeId = resultSet.getInt("mealTimeId");

            Menu menu = new Menu();
            Optional<MealTime> potentialMealTime = mealTimeDao.find(mealTimeId);
            if (potentialMealTime.isPresent()) {
                menu.setMealTime(potentialMealTime.get());
                menu.setMenuItems(new ArrayList<>());

                Optional<MenuItem> optionalMenuItem = menuItemDao.get(menuItemId);
                optionalMenuItem.ifPresent(menuItem -> menu.getMenuItems().add(menuItem));

                while (resultSet.next()) {
                    menuItemId = resultSet.getInt("id");

                    optionalMenuItem = menuItemDao.get(menuItemId);
                    optionalMenuItem.ifPresent(menuItem -> menu.getMenuItems().add(menuItem));
                }

                return Optional.of(menu);
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }
}
