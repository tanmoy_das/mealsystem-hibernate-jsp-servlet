<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Therap MealSystems</title>
</head>
<body>
<center>
    <h1>
        <a href="${pageContext.request.contextPath}">Therap MealSystems</a>
    </h1>
    <a href="${pageContext.request.contextPath}/logout">Logout</a>
</center>

<center>
    <a href="${pageContext.request.contextPath}/food">Available Food List</a>
    |
    <a href="${pageContext.request.contextPath}/menu">Daily Meal Menu</a>
</center>

<%--<br><br>--%>

<%--<center>--%>
<%--    <div>--%>
<%--        <form action="${pageContext.request.contextPath}/food" method="post">--%>
<%--            <label>--%>
<%--                Food Name:--%>
<%--                <input type="text" name="name">--%>
<%--            </label>--%>

<%--            <input type="submit" value="Add Food">--%>
<%--        </form>--%>
<%--    </div>--%>
<%--</center>--%>

<br><br><br>

<center>
    <div>
        <c:forEach items="${days}" var="day">
            <br>
            <div>
                <div style="border: #0e566c; border-style: solid">
                    <h1 style="display: inline-block"><b><u><i>${day.name.toUpperCase()}</i></u></b></h1>

                    <form style="display: inline-block; margin: 20px"
                          action="${pageContext.request.contextPath}/addMealTime" method="post">

                        <input type="hidden" name="day_id" value="${day.id}"/>


                        <label>
                            <input type="text" name="time" placeholder=" Meal Time">
                        </label>

                        <input type="submit" value="Add Meal Time">
                    </form>
                </div>

                <br>
                <div>
                    <c:forEach items="${day.mealTimes}" var="mealTime">
                        <div>
                            <br>
                            <h2 style="display: inline-block">${mealTime.time}</h2>

                            <form style="display: inline-block"
                                  action="${pageContext.request.contextPath}/deleteMealTime" method="post">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="meal_time_id" value="${mealTime.id}">
                                <input type="submit" value="X">
                            </form>


                            <form method="post"
                                  action="${pageContext.request.contextPath}/addMenu">
                                <input type="hidden" name="day_id" value="${day.id}">
                                <input type="hidden" name="time" value="${mealTime.time}">
                                <input type="text" name="name" placeholder="New Menu Name">
                                <input type="submit" value="Add Menu">
                            </form>
                        </div>

                        <br>

                        <c:forEach items="${mealTime.menus}" var="menu">

                            <div style="display: inline-block; border: #502aa1 2px; border-style: dashed; margin: 5px; padding: 5px">
                                <div>${menu.name}
                                    <form style="display: inline-block"
                                          action="${pageContext.request.contextPath}/deleteMenu" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="menu_id" value="${menu.id}">
                                        <input type="submit" value="X">
                                    </form>
                                </div>

                                <div>
                                    <form action="${pageContext.request.contextPath}/addMenuItem" method="post">
                                        <input type="hidden" name="menu_id" value="${menu.id}">
                                        <select name="food_id">
                                            <option selected disabled>Select Food</option>
                                            <c:forEach items="${foods}" var="food">
                                                <option value="${food.id}">${food.name}</option>
                                            </c:forEach>
                                        </select>
                                        <input type="text" name="maximum_amount" placeholder="maximum amount">
                                        <input type="submit" value="Add Menu Item">
                                    </form>
                                </div>

                                    <%--                        <c:if test="${menu.menuItems.size() > 0}">--%>
                                <table border="1" cellpadding="10" style=" margin: 10px">


                                    <tr>
                                        <th>Item</th>
                                        <th>Maximum Amount</th>
                                        <th>Action</th>
                                    </tr>

                                    <c:forEach items="${menu.menuItems}" var="menuItem">
                                        <tr>
                                            <td>${menuItem.food.name}</td>
                                            <td>${menuItem.maximumAmount}</td>
                                            <td>
                                                <form style="display: inline-block"
                                                      action="${pageContext.request.contextPath}/updateMenuItem"
                                                      method="post">
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="menu_item_id" value="${menuItem.id}">
                                                    <input type="text" placeholder="New Maximum Amount"
                                                           name="maximum_amount" size="18">
                                                    <input type="submit" value="Update">
                                                </form>

                                                <form style="display: inline-block"
                                                      action="${pageContext.request.contextPath}/deleteMenuItem"
                                                      method="post">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="menu_item_id" value="${menuItem.id}">
                                                    <input type="submit" value="Delete">
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                    <%--                        </c:if>--%>
                            </div>
                        </c:forEach>
                        <br>
                    </c:forEach>
                </div>
            </div>
            <br><br>
        </c:forEach>
    </div>
</center>


<link rel="stylesheet" type="text/css" href="lib/semantic-ui/semantic.min.css">
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="lib/semantic-ui/semantic.min.js"></script>
</body>
</html>
