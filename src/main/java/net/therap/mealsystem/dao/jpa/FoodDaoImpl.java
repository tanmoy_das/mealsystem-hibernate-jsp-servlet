package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.domain.Food;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class FoodDaoImpl implements FoodDao {

    private EntityManager em;

    public FoodDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<Food> find(int id) {
        Food food = em.find(Food.class, id);

        return Optional.ofNullable(food);
    }

    public Optional<Food> find(Food food) {
        TypedQuery<Food> query = em.createQuery("FROM Food f WHERE f.name = :name", Food.class);
        query.setParameter("name", food.getName());

        try {
            Food savedFood = query.getSingleResult();
            return Optional.of(savedFood);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Food> findAll() {
        TypedQuery<Food> query = em.createQuery("FROM Food", Food.class);

        return query.getResultList();
    }

    public Optional<Food> saveOrUpdate(Food food) {
        em.getTransaction().begin();

        Optional<Food> optionalFood = find(food);

        if (optionalFood.isPresent()) {
            Food savedFood = optionalFood.get();
            savedFood.setName(food.getName());

            food = em.merge(savedFood);
        } else {
            em.persist(food);
        }

        em.flush();
        em.getTransaction().commit();
        return Optional.ofNullable(food);
    }

    @Override
    public Optional<Food> save(Food food) {
        em.getTransaction().begin();

        em.persist(food);

        em.flush();
        em.getTransaction().commit();
        return Optional.of(food);
    }

    @Override
    public Optional<Food> findByName(String foodName) {
        TypedQuery<Food> query = em.createQuery("FROM Food WHERE name = :name", Food.class);
        query.setParameter("name", foodName);

        Food food = query.getSingleResult();

        return Optional.ofNullable(food);
    }

    @Override
    public void update(Food food) {
        em.getTransaction().begin();

        em.merge(food);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void delete(Food food) {
        em.getTransaction().begin();

        Optional<Food> optionalFood = find(food);
        optionalFood.ifPresent(value -> em.remove(value));

        em.flush();
        em.getTransaction().commit();
    }
}
