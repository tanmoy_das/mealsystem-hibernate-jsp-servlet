package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.domain.Food;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class FoodService {

    private FoodDao foodDao;

    public FoodService(FoodDao foodDao) {
        this.foodDao = foodDao;
    }

    public List<Food> getAllFoods() throws SQLException {
        List<Food> foods = foodDao.findAll();
        return foods;
    }

    public void deleteFood(String foodName) throws SQLException {
        Optional<Food> optionalFood = foodDao.findByName(foodName);
        if (!optionalFood.isPresent()) {
            System.err.println("Invalid Food Name");
        } else {
            Food food = optionalFood.get();
            foodDao.delete(food);
        }
    }

    public void addFood(String foodName) throws SQLException {
        Food food = new Food(foodName);
        foodDao.saveOrUpdate(food);
    }

    public void updateFood(int id, String name) throws SQLException {
        Optional<Food> optionalFood = foodDao.find(id);
        if (optionalFood.isPresent()) {
            Food food = optionalFood.get();
            food.setName(name);
            foodDao.saveOrUpdate(food);
        }
    }

    public void deleteFood(int id) throws SQLException {
        Optional<Food> optionalFood = foodDao.find(id);
        if (optionalFood.isPresent()) {
            Food food = optionalFood.get();

            foodDao.delete(food);
        }
    }
}
