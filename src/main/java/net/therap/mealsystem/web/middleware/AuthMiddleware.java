package net.therap.mealsystem.web.middleware;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.dao.jpa.UserDaoImpl;
import net.therap.mealsystem.domain.User;
import net.therap.mealsystem.service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
public class AuthMiddleware {

    private EntityManagerFactory emf;

    public AuthMiddleware() {
        emf = Persistence.createEntityManagerFactory("mealsystem");
    }

    public boolean isUser(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();

        if (cookies == null) {
            return false;
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                System.out.println(token);

                EntityManager em = emf.createEntityManager();

                UserDao userDao = new UserDaoImpl(em);
                UserService userService = new UserService(userDao);
                Optional<User> optionalUser = userService.getUserFromToken(token);

                em.close();

                return optionalUser.isPresent();
            }
        }
        return false;
    }

    public boolean isGuest(HttpServletRequest request, HttpServletResponse response) {
        return !isUser(request, response);
    }
}
