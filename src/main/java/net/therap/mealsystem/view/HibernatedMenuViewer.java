package net.therap.mealsystem.view;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;

import java.sql.SQLException;
import java.util.*;

/**
 * @author tanmoy.das
 * @since 3/16/20
 */
public class HibernatedMenuViewer implements MenuViewer {

    @Override
    public void view(Menu item) throws SQLException {
        ArrayList<Menu> items = new ArrayList<>();
        items.add(item);
        view(items);
    }

    @Override
    public void view(List<Menu> menus) throws SQLException {
        Set<Day> daySet = new HashSet<>();
        for (Menu menu : menus) {
            Day day = menu.getMealTime().getDay();
            daySet.add(day);
        }

        List<Day> days = new ArrayList<>(daySet);
        days.sort(Comparator.comparingInt(Day::getId));

        view(menus, days.toArray(new Day[0]));
    }

    @Override
    public void view(List<Menu> menus, Day[] days) {
        Map<Day, Set<MealTime>> mealTimesByDay = new HashMap<>();
        Map<MealTime, List<Menu>> menusByMealTime = new HashMap<>();

        for (Day day : days) {
            mealTimesByDay.put(day, new HashSet<>());
        }

        for (Menu menu : menus) {
            MealTime mealTime = menu.getMealTime();
            Day day = mealTime.getDay();

            mealTimesByDay.get(day).add(menu.getMealTime());

            if (!menusByMealTime.containsKey(mealTime)) {
                menusByMealTime.put(mealTime, new ArrayList<>());
            }

            menusByMealTime.get(mealTime).add(menu);
        }

        for (Day day : days) {
            System.out.println(day.getName());
            System.out.println("---------------------");

            Set<MealTime> mealTimeSet = mealTimesByDay.get(day);
            List<MealTime> mealTimeList = new ArrayList<>(mealTimeSet);
            mealTimeList.sort(Comparator.comparingInt(MealTime::getId));

            for (MealTime mealTime : mealTimeList) {
                System.out.print(mealTime.getTime());
                System.out.print(" menu:");

                List<Menu> menuList = menusByMealTime.get(mealTime);
                menuList.sort(Comparator.comparingInt(Menu::getId));

                for (Menu menu : menuList) {
                    System.out.printf("%n%-2s %s: ", "", menu.getName());

                    boolean firstItem = true;
                    for (MenuItem menuItem : menu.getMenuItems()) {
                        if (!firstItem) {
                            System.out.print(", ");
                        } else {
                            firstItem = false;
                        }

                        String foodName = menuItem.getFood().getName();
                        String maxAmount = menuItem.getMaximumAmount();

                        System.out.printf("%s (%s)", foodName, maxAmount);
                    }
                }
                System.out.println();
            }

            System.out.println();
            System.out.println();
        }
    }
}
