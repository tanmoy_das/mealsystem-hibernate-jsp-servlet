package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/24/20
 */
public class MenuItemNotValidException extends RuntimeException {
    public MenuItemNotValidException() {
        super("Menu Item does not exist in the specified menu");
    }
}
