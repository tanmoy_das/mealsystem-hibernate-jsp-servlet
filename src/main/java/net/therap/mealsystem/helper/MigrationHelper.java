package net.therap.mealsystem.helper;

import net.therap.mealsystem.migration.FoodMigration;
import net.therap.mealsystem.migration.MealTimeMigration;
import net.therap.mealsystem.migration.MenuItemMigration;
import net.therap.mealsystem.migration.Migratable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class MigrationHelper {

    public static void migrateAll(Connection connection) throws SQLException {
        List<Migratable> migrations = new ArrayList<>();

        migrations.add(new FoodMigration());
        migrations.add(new MealTimeMigration());
        migrations.add(new MenuItemMigration());

        for (Migratable migration : migrations) {
            migration.migrate(connection);
        }
    }

    public static void rollbackAll(Connection connection) throws SQLException {
        List<Migratable> migrations = new ArrayList<>();

        migrations.add(new FoodMigration());
        migrations.add(new MealTimeMigration());
        migrations.add(new MenuItemMigration());

        for (Migratable migration : migrations) {
            migration.rollback(connection);
        }
    }

    public static void refreshAll(Connection connection) throws SQLException {
        List<Migratable> migrations = new ArrayList<>();

        migrations.add(new FoodMigration());
        migrations.add(new MealTimeMigration());
        migrations.add(new MenuItemMigration());

        for (Migratable migration : migrations) {
            migration.refresh(connection);
        }
    }
}
