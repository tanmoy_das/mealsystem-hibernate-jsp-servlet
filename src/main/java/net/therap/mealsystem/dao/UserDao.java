package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;

import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/25/20
 */
public interface UserDao {

    Optional<User> findUserByToken(String tokenTxt);

    void save(User user);

    User find(int id);

    Optional<User> find(User u);

    void addToken(User user, Token token);

    void update(User user);

    void saveOrUpdate(User user);

    void deleteToken(User user, Token token);
}
