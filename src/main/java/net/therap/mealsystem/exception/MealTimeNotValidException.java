package net.therap.mealsystem.exception;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class MealTimeNotValidException extends RuntimeException {
    public MealTimeNotValidException() {
        super("Meal time does not exist in the database");
    }
}
