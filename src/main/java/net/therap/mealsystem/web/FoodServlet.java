package net.therap.mealsystem.web;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.jpa.FoodDaoImpl;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.service.FoodService;
import net.therap.mealsystem.util.HibernateUtil;
import net.therap.mealsystem.web.middleware.AuthMiddleware;
import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@WebServlet(
        name = "FoodServlet",
        urlPatterns = {"/food"}
)
public class FoodServlet extends HttpServlet {

    private FoodService foodService;

    public FoodServlet() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        FoodDao foodDao = new FoodDaoImpl(session);
        this.foodService = new FoodService(foodDao);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AuthMiddleware authMiddleware = new AuthMiddleware();

        if (authMiddleware.isGuest(req, resp)) {
            resp.sendRedirect(req.getContextPath() + "");
        } else {
            super.service(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Food> foods = foodService.getAllFoods();
            req.setAttribute("foods", foods);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("food.jsp");
            requestDispatcher.forward(req, resp);

        } catch (SQLException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("_method");

        if (method == null) {

            String name = req.getParameter("name");
            try {
                foodService.addFood(name);
                resp.sendRedirect(req.getContextPath() + "/food");

            } catch (SQLException e) {
                resp.sendError(500, e.getMessage());
                e.printStackTrace();
            }
        } else if (method.toUpperCase().equals("PUT")) {
            doPut(req, resp);
        } else if (method.toUpperCase().equals("DELETE")) {
            doDelete(req, resp);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");

        try {
            foodService.updateFood(id, name);
            resp.sendRedirect(req.getContextPath() + "/food");

        } catch (SQLException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));

        try {
            foodService.deleteFood(id);
            resp.sendRedirect(req.getContextPath() + "/food");

        } catch (SQLIntegrityConstraintViolationException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        } catch (SQLException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }
    }
}

