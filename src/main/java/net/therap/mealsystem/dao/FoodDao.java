package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Food;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface FoodDao {
    Optional<Food> find(int id) throws SQLException;

    List<Food> findAll() throws SQLException;

    Optional<Food> save(Food food) throws SQLException;

    Optional<Food> saveOrUpdate(Food food) throws SQLException;

    Optional<Food> findByName(String name) throws SQLException;

    void update(Food food) throws SQLException;

    void delete(Food food) throws SQLException;
}
