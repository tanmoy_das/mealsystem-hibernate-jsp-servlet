package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
public class UserDaoImpl implements UserDao {

    private EntityManager em;

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<User> findUserByToken(String tokenTxt) {
        em.getTransaction().begin();

        String sql = "SELECT t FROM Token t WHERE t.value = :token";
        Query query = em.createQuery(sql);
        query.setParameter("token", tokenTxt);

        User user = null;
        try {
            Token token = (Token) query.getSingleResult();
            user = token.getUser();
        } catch (NoResultException ignored) {
        }

        em.flush();
        em.getTransaction().commit();

        return Optional.ofNullable(user);
    }

    @Override
    public void save(User user) {
        em.getTransaction().begin();

        em.persist(user);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public User find(int id) {
        return em.find(User.class, id);
    }

    @Override
    public Optional<User> find(User u) {
        em.getTransaction().begin();

        String sql = "SELECT u FROM User u WHERE u.email = :email AND u.password = :password";
        Query query = em.createQuery(sql);

        query.setParameter("email", u.getEmail());
        query.setParameter("password", u.getPassword());

        User user = null;
        try {
            user = (User) query.getSingleResult();
        } catch (NoResultException ignored) {
        }

        em.flush();
        em.getTransaction().commit();

        return Optional.ofNullable(user);
    }

    @Override
    public void addToken(User user, Token token) {
        em.getTransaction().begin();

        token.setUser(user);
        user.getTokens().add(token);

        em.merge(user);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void update(User user) {
        em.getTransaction().begin();

        em.merge(user);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void saveOrUpdate(User user) {
        Optional<User> optionalUser = find(user);

        em.getTransaction().begin();

        if (optionalUser.isPresent()) {
            em.merge(user);
        } else {
            em.persist(user);
        }

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void deleteToken(User user, Token token) {
        em.getTransaction().begin();

        user.getTokens().remove(token);
        em.remove(token);
        em.merge(user);

        em.flush();
        em.getTransaction().commit();
    }
}
