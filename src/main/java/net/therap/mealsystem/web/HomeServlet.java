package net.therap.mealsystem.web;

import net.therap.mealsystem.web.middleware.AuthMiddleware;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@WebServlet(
        name = "HomeServlet",
        urlPatterns = {""}
)
public class HomeServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        AuthMiddleware authMiddleware = new AuthMiddleware();

        if (authMiddleware.isGuest(req, resp)) {
            resp.sendRedirect(req.getContextPath() + "/login");
        } else {
            super.service(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("home.jsp");
        requestDispatcher.forward(req, resp);

    }
}

