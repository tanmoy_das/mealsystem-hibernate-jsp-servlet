package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.exception.DayNotValidException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class DayService {

    private DayDao dayDao;

    public DayService(DayDao dayDao) {
        this.dayDao = dayDao;
    }

    public Day getDayByName(String dayName) throws SQLException, DayNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (optionalDay.isPresent()) {
            return optionalDay.get();
        } else {
            throw new DayNotValidException();
        }
    }

    public void initializeDays() throws SQLException {
        String[] dayNames = Day.DAY_NAMES;

        List<Day> days = new ArrayList<>();
        for (int i = 0; i < dayNames.length; i++) {
            days.add(new Day(i + 1, dayNames[i]));
        }

        for (Day day : days) {
            dayDao.saveOrUpdate(day);
        }
    }

    public List<Day> getAllDays() throws SQLException {
        return dayDao.findAll();
    }
}
