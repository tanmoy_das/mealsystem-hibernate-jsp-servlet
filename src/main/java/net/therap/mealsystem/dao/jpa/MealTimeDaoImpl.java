package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class MealTimeDaoImpl implements MealTimeDao {

    private EntityManager em;

    public MealTimeDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<MealTime> find(int id) {
        MealTime mealTime = em.find(MealTime.class, id);

        return Optional.ofNullable(mealTime);
    }

    public Optional<MealTime> find(MealTime mealTime) {

        TypedQuery<MealTime> query = em.createQuery("FROM MealTime WHERE day = :day AND time = :time", MealTime.class);
        query.setParameter("day", mealTime.getDay());
        query.setParameter("time", mealTime.getTime());

        try {
            MealTime mealTime1 = query.getSingleResult();
            return Optional.of(mealTime1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<MealTime> findAll() {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime ORDER BY id", MealTime.class);
        List<MealTime> resultList = query.getResultList();

        resultList.sort(Comparator.comparingInt(MealTime::getId));
        return resultList;
    }

    @Override
    public void saveOrUpdate(MealTime mealTime) {
        Optional<MealTime> optionalMealTime = find(mealTime);

        if (optionalMealTime.isPresent()) {
            MealTime savedMealTime = optionalMealTime.get();
            em.merge(savedMealTime);
        } else {
            em.persist(mealTime);
        }
    }

    @Override
    public Optional<MealTime> save(MealTime mealTime) {
        em.getTransaction().begin();

        em.persist(mealTime);

        em.flush();
        em.getTransaction().commit();
        return Optional.of(mealTime);
    }

    @Override
    public Optional<MealTime> findByDayTime(Day day, String time) {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime mealTime WHERE mealTime.day = :day AND time = :time", MealTime.class);
        query.setParameter("day", day);
        query.setParameter("time", time);

        MealTime mealTime;
        try {
            mealTime = query.getSingleResult();
        } catch (NoResultException ignored) {
            mealTime = null;
        }

        return Optional.ofNullable(mealTime);
    }

    public List<MealTime> findByDay(Day day) {
        TypedQuery<MealTime> query = em.createQuery("FROM MealTime WHERE day = :day", MealTime.class);
        query.setParameter("day", day);

        return query.getResultList();
    }

    @Override
    public void update(MealTime mealTime) {
        em.getTransaction().begin();

        em.merge(mealTime);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void delete(MealTime mealTime) {
        em.getTransaction().begin();

        mealTime.getDay().getMealTimes().remove(mealTime);
        em.merge(mealTime.getDay());

        Optional<MealTime> optionalMealTime = find(mealTime);
        optionalMealTime.ifPresent(time -> em.remove(time));

        em.flush();
        em.getTransaction().commit();
    }
}
