package net.therap.mealsystem.web;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.dao.jpa.UserDaoImpl;
import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.service.UserService;
import net.therap.mealsystem.web.middleware.AuthMiddleware;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@WebServlet(
        name = "AuthServlet",
        urlPatterns = {
                "/login",
                "/register",
                "/logout"
        }
)
public class AuthServlet extends HttpServlet {

    private final EntityManagerFactory emf;

    public AuthServlet() {
        emf = Persistence.createEntityManagerFactory("mealsystem");
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AuthMiddleware authMiddleware = new AuthMiddleware();

        String loginPath = req.getContextPath() + "/login";
        String registerPath = req.getContextPath() + "/register";
        String logoutPath = req.getContextPath() + "/logout";

        List<String> guestPaths = new ArrayList<>();
        guestPaths.add(loginPath);
        guestPaths.add(registerPath);

        List<String> userPaths = new ArrayList<>();
        userPaths.add(logoutPath);

        String uri = req.getContextPath() + req.getRequestURI();

        if (authMiddleware.isUser(req, resp) && guestPaths.contains(uri)) {
            resp.sendRedirect(req.getContextPath());
        } else if (authMiddleware.isGuest(req, resp) && userPaths.contains(uri)) {
            resp.sendRedirect(req.getContextPath());
        } else {
            super.service(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginPath = req.getContextPath() + "/login";
        String registerPath = req.getContextPath() + "/register";
        String logoutPath = req.getContextPath() + "/logout";

        if (req.getRequestURI().equals(loginPath)) {

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(req, resp);

        } else if (req.getRequestURI().equals(registerPath)) {

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("register.jsp");
            requestDispatcher.forward(req, resp);

        } else if (req.getRequestURI().equals(logoutPath)) {

            handleLogout(req, resp);

        } else {

            resp.getWriter().println(req.getRequestURI());
            resp.getWriter().close();

        }
    }

    private void handleLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        EntityManager em = emf.createEntityManager();

        UserDao userDao = new UserDaoImpl(em);
        UserService userService = new UserService(userDao);

        Optional<Cookie> cookieOptional = Arrays.stream(req.getCookies())
                .filter(c -> c.getName().equals("token"))
                .findAny();

        if (cookieOptional.isPresent()) {
            Cookie cookie = cookieOptional.get();

            String tokenTxt = cookie.getValue();
            userService.invalidateToken(tokenTxt);

            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }

        resp.sendRedirect(req.getContextPath());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String loginPath = req.getContextPath() + "/login";
        String registerPath = req.getContextPath() + "/register";

        if (req.getRequestURI().equals(loginPath)) {
            handleLogin(req, resp);
        } else if (req.getRequestURI().equals(registerPath)) {
            handleRegister(req, resp);
        } else {

            resp.getWriter().println(req.getRequestURI());
            resp.getWriter().close();

        }
    }

    private void handleRegister(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        try {
            password = getHash(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        EntityManager em = emf.createEntityManager();

        UserDao userDao = new UserDaoImpl(em);
        UserService userService = new UserService(userDao);

        try {
            if (!userService.loginUser(email, password).isPresent()) {
                userService.registerUser(email, password);
                resp.sendRedirect(req.getContextPath());
            } else {
                resp.sendError(500, "User with this email alreaady exists");
            }
        } catch (Exception e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }

        em.close();
    }

    private String getHash(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(
                password.getBytes(StandardCharsets.UTF_8));
        password = bytesToHex(encodedhash);
        return password;
    }

    private void handleLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        try {
            password = getHash(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        EntityManager em = emf.createEntityManager();

        UserDao userDao = new UserDaoImpl(em);
        UserService userService = new UserService(userDao);

        Optional<Token> optionalToken = userService.loginUser(email, password);

        if (optionalToken.isPresent()) {
            Cookie cookie = new Cookie("token", optionalToken.get().getValue());
            cookie.setMaxAge(30 * 24 * 60 * 60);
            resp.addCookie(cookie);
        }

        resp.sendRedirect(req.getContextPath());
    }
}
