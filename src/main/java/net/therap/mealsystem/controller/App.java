package net.therap.mealsystem.controller;

import net.therap.mealsystem.util.CliParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public abstract class App {

    public void initialize() throws SQLException {

    }

    public void run(String[] args) throws SQLException {
        try {
            CommandLine cmd = parseArgs(args);

            if (cmd.hasOption("addFood")) {
                String foodName = cmd.getOptionValue("addFood");
                addFood(foodName);
            }

            if (cmd.hasOption("deleteFood")) {
                String foodName = cmd.getOptionValue("deleteFood");
                deleteFood(foodName);
            }

            if (cmd.hasOption("showFoods") || cmd.hasOption("viewFoods")) {
                viewFoods();
            }

            if (cmd.hasOption("addItem") && cmd.hasOption("day") && cmd.hasOption("time")
                    && cmd.hasOption("maxAmount")) {
                String foodName = cmd.getArgs()[0];
                String dayName = cmd.getOptionValue("day");
                String time = cmd.getOptionValue("time");
                String maxAmount = cmd.getOptionValue("maxAmount");

                if (cmd.hasOption("menuName")) {
                    String menuName = cmd.getOptionValue("menuName");
                    addItem(foodName, dayName, time, maxAmount, menuName);
                } else {
                    addItem(foodName, dayName, time, maxAmount);
                }
            }

            if (cmd.hasOption("updateItem") && cmd.hasOption("day") && cmd.hasOption("time")
                    && cmd.hasOption("maxAmount")) {
                String foodName = cmd.getArgs()[0];
                String dayName = cmd.getOptionValue("day");
                String time = cmd.getOptionValue("time");
                String maxAmount = cmd.getOptionValue("maxAmount");

                if (cmd.hasOption("menuName")) {
                    String menuName = cmd.getOptionValue("menuName");
                    updateItem(foodName, dayName, time, maxAmount, menuName);
                } else {
                    updateItem(foodName, dayName, time, maxAmount);
                }
            }

            if (cmd.hasOption("removeItem") && cmd.hasOption("day") && cmd.hasOption("time")) {
                String foodName = cmd.getArgs()[0];
                String dayName = cmd.getOptionValue("day");
                String time = cmd.getOptionValue("time");

                if (cmd.hasOption("menuName")) {
                    String menuName = cmd.getOptionValue("menuName");
                    removeItem(foodName, dayName, time, menuName);
                } else {
                    removeItem(foodName, dayName, time);
                }
            }

            if (cmd.hasOption("viewMenu") || cmd.hasOption("showMenu")) {
                if (cmd.hasOption("day")) {
                    String dayName = cmd.getOptionValue("day");
                    showMenu(dayName);
                } else {
                    showMenu();
                }
            }

            if (cmd.hasOption("migrate")) {
                migrateAllMigrations();
            }

            if (cmd.hasOption("migrate_rollback")) {
                rollbackAllMigrations();
            }

            if (cmd.hasOption("migrate_refresh")) {
                refreshAllMigrations();
            }

            if (cmd.hasOption("addBreakfastLunch")) {
                addBreakfastLunch();
            }

        } catch (ParseException e) {
            System.err.println(e.getMessage());
        }
    }

    public void addBreakfastLunch() throws SQLException {

    }

    public CommandLine parseArgs(String[] args) throws ParseException {
        CliParser cliParser = new CliParser();

        cliParser.addOption("addFood", true, "Add Food to Food List");
        cliParser.addOption("deleteFood", true, "Delete Food from Food List");
        cliParser.addOption("showFoods", false, "Show All Foods");
        cliParser.addOption("viewFoods", false, "Show All Foods");

        cliParser.addOption("day", true, "Day");
        cliParser.addOption("time", true, "Time");
        cliParser.addOption("maxAmount", true, "Maximum amount per person");
        cliParser.addOption("menuName", true, "Menu name (default \"general\"");

        cliParser.addOption("addItem", false, "Add Item to menu");
        cliParser.addOption("removeItem", false, "Remove Item from menu");
        cliParser.addOption("updateItem", false, "Update Item from menu");

        cliParser.addOption("viewMenu", false, "View Menu");
        cliParser.addOption("showMenu", false, "View Menu");

        cliParser.addOption("migrate", false, "Migrate the Tables");
        cliParser.addOption("migrate_rollback", false, "Rollback Migrations");
        cliParser.addOption("migrate_refresh", false, "Refresh Migrations");

        cliParser.addOption("addBreakfastLunch", false, "Add Breakfast and Lunch every day");
        cliParser.addOption("addMealTime", false, "Add new meal time");
        cliParser.addOption("deleteMealTime", false, "Add new meal time");

        cliParser.addOption("addMenu", false, "Add new meal time");
        cliParser.addOption("deleteMenu", false, "Add new meal time");


        return cliParser.parseCliArguments(args);
    }

    public abstract void refreshAllMigrations() throws SQLException;

    public abstract void rollbackAllMigrations() throws SQLException;

    public abstract void showMenu() throws SQLException;

    public abstract void migrateAllMigrations() throws SQLException;

    public abstract void addItem(String foodName, String dayName, String time, String maxAmount) throws SQLException;

    public void addItem(String foodName, String dayName, String time, String maxAmount, String menuName) throws SQLException {
        addItem(foodName, dayName, time, maxAmount);
    }

    public abstract void updateItem(String foodName, String dayName, String time, String maxAmount) throws SQLException;

    public void updateItem(String foodName, String dayName, String time, String maxAmount, String menuName) throws SQLException {
        updateItem(foodName, dayName, time, maxAmount);
    }

    public abstract void removeItem(String foodName, String dayName, String time) throws SQLException;

    public void removeItem(String foodName, String dayName, String time, String menuName) throws SQLException {
        removeItem(foodName, dayName, time);
    }

    public abstract void showMenu(String dayName) throws SQLException;

    public abstract void viewFoods() throws SQLException;

    public abstract void deleteFood(String foodName) throws SQLException;

    public abstract void addFood(String foodName) throws SQLException;

    public void shutdown() throws SQLException {
    }
}
