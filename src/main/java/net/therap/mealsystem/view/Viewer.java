package net.therap.mealsystem.view;

import java.sql.SQLException;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public interface Viewer<T> {

    void view(T item) throws SQLException;

    void view(List<T> items) throws SQLException;
}
