package net.therap.mealsystem.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
@Entity
@Table(name = "meal_times", uniqueConstraints = @UniqueConstraint(columnNames = {"day_id", "time"}))
public class MealTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "day_id")
    private Day day;

    private String time;

    @OneToMany(mappedBy = "mealTime", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Menu> menus;

    public MealTime() {
        this.menus = new ArrayList<>();
    }

    public MealTime(Day day, String time) {
        this();
        this.day = day;
        this.time = time;
    }

    public MealTime(int id, Day day, String time) {
        this();
        this.id = id;
        this.day = day;
        this.time = time;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "MealTime{" +
                "day=" + day +
                ", time='" + time + '\'' +
                '}';
    }
}
