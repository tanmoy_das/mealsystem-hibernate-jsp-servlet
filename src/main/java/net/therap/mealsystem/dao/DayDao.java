package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public interface DayDao {
    Optional<Day> find(int id) throws SQLException;

    List<Day> findAll() throws SQLException;

    Optional<Day> saveOrUpdate(Day day) throws SQLException;

    Optional<Day> save(Day day) throws SQLException;

    Optional<Day> findByName(String dayName) throws SQLException;
}
