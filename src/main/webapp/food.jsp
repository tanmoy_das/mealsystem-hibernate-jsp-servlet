<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Therap MealSystems</title>
</head>
<body>
<center>
    <h1>
        <a href="${pageContext.request.contextPath}">Therap MealSystems</a>
    </h1>
    <a href="${pageContext.request.contextPath}/logout">Logout</a>
</center>

<center>
    <a href="${pageContext.request.contextPath}/food">Available Food List</a>
    |
    <a href="${pageContext.request.contextPath}/menu">Daily Meal Menu</a>
</center>

<br><br>

<center>
    <div>
        <form action="${pageContext.request.contextPath}/food" method="post">
            <label>
                Food Name:
                <input type="text" name="name">
            </label>

            <input type="submit" value="Add Food">
        </form>
    </div>
</center>

<br><br><br>

<center>
    <table border="1" cellpadding="10">
        <tr>
            <th>Name</th>
            <th COLSPAN="2">Actions</th>
        </tr>

        <c:forEach items="${foods}" var="food">
            <tr>
                <td>${food.name}</td>
                <td>
                    <div>
                        <form action="${pageContext.request.contextPath}/food" method="post">
                            <input type="hidden" name="id" value="${food.id}">
                            <input type="hidden" name="_method" value="put">
                            <label>
                                <input size="11" type="text" name="name" placeholder="New Name">
                            </label>
                            <input type="submit" value="Update">
                        </form>
                    </div>
                </td>
                <td>
                    <div>
                        <form action="${pageContext.request.contextPath}/food" method="post">
                            <input type="hidden" name="id" value="${food.id}">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="submit" value="Delete">
                        </form>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
</center>


<link rel="stylesheet" type="text/css" href="lib/semantic-ui/semantic.min.css">
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="lib/semantic-ui/semantic.min.js"></script>
</body>
</html>
