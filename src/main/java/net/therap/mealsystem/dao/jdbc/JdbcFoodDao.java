package net.therap.mealsystem.dao.jdbc;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.mapper.FoodMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
public class JdbcFoodDao implements FoodDao {

    private Connection connection;
    private FoodMapper foodMapper;

    public JdbcFoodDao(Connection connection, FoodMapper foodMapper) throws SQLException {
        this.connection = connection;
        this.foodMapper = foodMapper;
    }

    @Override
    public Optional<Food> find(int id) throws SQLException {
        String SQL_SELECT = "SELECT * FROM foods WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setLong(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        return foodMapper.map(resultSet);
    }

    @Override
    public List<Food> findAll() throws SQLException {
        List<Food> foods = new ArrayList<>();
        String SQL_SELECT = "SELECT * FROM foods";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);

        ResultSet resultSet = preparedStatement.executeQuery();
        while (!resultSet.isAfterLast()) {
            Optional<Food> optionalFood = foodMapper.map(resultSet);
            optionalFood.ifPresent(foods::add);
        }

        return foods;
    }

    @Override
    public Optional<Food> save(Food food) throws SQLException {
        String SQL_INSERT = "INSERT INTO foods (name) VALUES (?)";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
        preparedStatement.setString(1, food.getName());
        preparedStatement.executeUpdate();

        return findByName(food.getName());
    }

    @Override
    public Optional<Food> saveOrUpdate(Food food) throws SQLException {
        Optional<Food> optionalFood = findByName(food.getName());

        if (optionalFood.isPresent()) {
            update(optionalFood.get());
        } else {
            save(food);
        }

        return findByName(food.getName());
    }

    @Override
    public Optional<Food> findByName(String name) throws SQLException {
        String SQL_SELECT = "SELECT * FROM foods WHERE name = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setString(1, name);

        ResultSet resultSet = preparedStatement.executeQuery();
        return foodMapper.map(resultSet);
    }

    @Override
    public void update(Food food) throws SQLException {
        String SQL_UPDATE = "UPDATE foods SET name = ?, id = ? WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);
        preparedStatement.setString(1, food.getName());
        preparedStatement.setInt(2, food.getId());
        preparedStatement.setInt(3, food.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(Food food) throws SQLException {
        String SQL_DELETE = "DELETE FROM foods WHERE id = ?";

        PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE);
        preparedStatement.setInt(1, food.getId());
        preparedStatement.executeUpdate();
    }
}
