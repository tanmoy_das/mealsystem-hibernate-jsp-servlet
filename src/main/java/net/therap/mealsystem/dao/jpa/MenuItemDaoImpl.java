package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.domain.MenuItem;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class MenuItemDaoImpl {

    private EntityManager em;

    public MenuItemDaoImpl(EntityManager em) {
        this.em = em;
    }

    public Optional<MenuItem> find(int id) {
        MenuItem menuItem = em.find(MenuItem.class, id);

        return Optional.ofNullable(menuItem);
    }

    private Optional<Object> find(MenuItem menuItem) {
        TypedQuery<MenuItem> query = em.createQuery("FROM MenuItem WHERE menu = :menu AND food = :food", MenuItem.class);
        query.setParameter("menu", menuItem.getMenu());
        query.setParameter("food", menuItem.getFood());

        try {
            MenuItem menuItem1 = query.getSingleResult();
            return Optional.of(menuItem1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public List<MenuItem> findAll() {
        TypedQuery<MenuItem> query = em.createQuery("FROM MenuItem ORDER BY id", MenuItem.class);

        return query.getResultList();
    }


    public Optional<MenuItem> save(MenuItem menuItem) {
        em.getTransaction().begin();

        em.persist(menuItem);

        em.flush();
        em.getTransaction().commit();
        return Optional.ofNullable(menuItem);
    }

    public void update(MenuItem menuItem) {
        em.getTransaction().begin();

        em.merge(menuItem);

        em.flush();
        em.getTransaction().commit();
    }

    public void delete(MenuItem menuItem) {
        em.getTransaction().begin();

        find(menuItem).ifPresent(menuItem1 -> em.remove(menuItem1));

        em.flush();
        em.getTransaction().commit();
    }
}
