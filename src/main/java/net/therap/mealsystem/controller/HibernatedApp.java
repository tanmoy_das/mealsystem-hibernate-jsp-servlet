package net.therap.mealsystem.controller;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.jpa.*;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exception.*;
import net.therap.mealsystem.service.DayService;
import net.therap.mealsystem.service.FoodService;
import net.therap.mealsystem.service.MealTimeService;
import net.therap.mealsystem.service.MenuService;
import net.therap.mealsystem.view.FoodViewer;
import net.therap.mealsystem.view.HibernatedMenuViewer;
import net.therap.mealsystem.view.MenuViewer;
import net.therap.mealsystem.view.Viewer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class HibernatedApp extends App {

    EntityManagerFactory emf;
    EntityManager em;

    private DayService dayService;
    private FoodService foodService;
    private MealTimeService mealTimeService;
    private MenuService menuService;


    public HibernatedApp() {
        emf = Persistence.createEntityManagerFactory("mealsystem");
        em = emf.createEntityManager();

        FoodDao foodDao = new FoodDaoImpl(em);
        MealTimeDao mealTimeDao = new MealTimeDaoImpl(em);
        MenuItemDaoImpl menuItemDao = new MenuItemDaoImpl(em);
        MenuDao menuDao = new MenuDaoImpl(em, mealTimeDao);
        DayDao dayDao = new DayDaoImpl(em);

        this.dayService = new DayService(dayDao);
        this.foodService = new FoodService(foodDao);
        this.mealTimeService = new MealTimeService(dayDao, mealTimeDao, menuDao, menuItemDao);
        this.menuService = new MenuService(dayDao, foodDao, mealTimeDao, menuDao, menuItemDao);
    }

    @Override
    public void initialize() throws SQLException {
//        session.beginTransaction();
        dayService.initializeDays();

//        session.getTransaction().commit();
    }

    @Override
    public void run(String[] args) throws SQLException {
//        session.beginTransaction();

        super.run(args);
        try {
            CommandLine cmd = parseArgs(args);


            if (cmd.hasOption("addMealTime")) {
                if (cmd.hasOption("day") && cmd.hasOption("time")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");

                    addMealTime(dayName, time);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("deleteMealTime")) {
                if (cmd.hasOption("day") && cmd.hasOption("time")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");

                    deleteMealTime(dayName, time);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("addMenu")) {
                if (cmd.hasOption("day") && cmd.hasOption("time") && cmd.hasOption("menuName")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String menuName = cmd.getOptionValue("menuName");

                    addMenu(dayName, time, menuName);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

            if (cmd.hasOption("deleteMenu")) {
                if (cmd.hasOption("day") && cmd.hasOption("time") && cmd.hasOption("menuName")) {
                    String dayName = cmd.getOptionValue("day");
                    String time = cmd.getOptionValue("time");
                    String menuName = cmd.getOptionValue("menuName");

                    deleteMenu(dayName, time, menuName);
                } else {
                    System.err.println("Please provide Day and Time");
                }
            }

        } catch (ParseException | DayNotValidException | DuplicateEntryException | MealTimeNotValidException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
//        session.getTransaction().commit();
    }

    private void addMenu(String dayName, String time, String menuName) throws SQLException, DayNotValidException, MealTimeNotValidException, DuplicateEntryException {
        menuService.addMenu(dayName, time, menuName);
    }

    private void deleteMenu(String dayName, String time, String menuName) throws SQLException {
        menuService.deleteMenu(dayName, time, menuName);
    }

    private void addMealTime(String dayName, String time) throws SQLException, DayNotValidException {
        mealTimeService.addMealTime(dayName, time);
    }

    private void deleteMealTime(String dayName, String time) throws SQLException {
        mealTimeService.deleteMealTime(dayName, time);
    }

    @Override
    public void addBreakfastLunch() throws SQLException {
        mealTimeService.addBreakfastLunch();
    }

    @Override
    @Deprecated
    public void migrateAllMigrations() {
    }

    @Override
    @Deprecated
    public void rollbackAllMigrations() {
    }

    @Override
    @Deprecated
    public void refreshAllMigrations() {
    }

    @Override
    public void showMenu() throws SQLException {
        List<Menu> menus = menuService.getAllMenus();

        MenuViewer menuViewer = new HibernatedMenuViewer();
        menuViewer.view(menus);
    }

    @Override
    public void showMenu(String dayName) throws SQLException {
        try {
            List<Menu> menus = menuService.getMenusByDayName(dayName);
            Day day = dayService.getDayByName(dayName);

            MenuViewer menuViewer = new HibernatedMenuViewer();
            menuViewer.view(menus, new Day[]{day});

        } catch (DayNotValidException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void removeItem(String foodName, String dayName, String time) throws SQLException {
        removeItem(foodName, dayName, time, Menu.DEFAULT_NAME);
    }

    @Override
    public void removeItem(String foodName, String dayName, String time, String menuName) throws SQLException {
        try {
            menuService.removeItem(foodName, dayName, time, menuName);
        } catch (MenuNotValidException | MealTimeNotValidException | DayNotValidException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        updateItem(foodName, dayName, time, maxAmount, Menu.DEFAULT_NAME);
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount, String mealName) throws SQLException {
        try {
            menuService.updateItem(foodName, dayName, time, maxAmount, mealName);
        } catch (MealTimeNotValidException | DayNotValidException | MenuNotValidException | MenuItemNotValidException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        addItem(foodName, dayName, time, maxAmount, Menu.DEFAULT_NAME);
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount, String mealName) throws SQLException {
        try {
            menuService.addItem(foodName, dayName, time, maxAmount, mealName);
        } catch (DayNotValidException | MealTimeNotValidException | MenuNotValidException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void viewFoods() throws SQLException {
        List<Food> foods = foodService.getAllFoods();

        Viewer<Food> viewer = new FoodViewer();
        viewer.view(foods);
    }

    @Override
    public void deleteFood(String foodName) throws SQLException {
        foodService.deleteFood(foodName);
    }

    @Override
    public void addFood(String foodName) throws SQLException {
        foodService.addFood(foodName);
    }

    @Override
    public void shutdown() {
        if (em != null && em.isOpen()) {
            em.close();
        }

        if (emf != null && emf.isOpen()) {
            emf.close();
        }
    }
}
