package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.jpa.MenuItemDaoImpl;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.domain.MenuItem;
import net.therap.mealsystem.exception.DayNotValidException;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class MealTimeService {

    private DayDao dayDao;
    private MealTimeDao mealTimeDao;
    private MenuDao menuDao;
    private MenuItemDaoImpl menuItemDao;

    public MealTimeService(DayDao dayDao, MealTimeDao mealTimeDao, MenuDao menuDao, MenuItemDaoImpl menuItemDao) {
        this.dayDao = dayDao;
        this.mealTimeDao = mealTimeDao;
        this.menuDao = menuDao;
        this.menuItemDao = menuItemDao;
    }

    public void addMealTime(String dayName, String time) throws SQLException, DayNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        addMealTime(time, optionalDay);
    }

    public void addMealTime(int id, String time) throws SQLException, DayNotValidException {
        Optional<Day> optionalDay = dayDao.find(id);

        addMealTime(time, optionalDay);
    }

    private void addMealTime(String time, Optional<Day> optionalDay) throws SQLException, DayNotValidException {
        if (!optionalDay.isPresent()) {
            throw new DayNotValidException();
        } else {
            Day day = optionalDay.get();

            MealTime mealTime = new MealTime(day, time);
            mealTimeDao.saveOrUpdate(mealTime);

            day.getMealTimes().add(mealTime);
            dayDao.saveOrUpdate(day);

            Menu menu = new Menu();
            menu.setMealTime(mealTime);
            menu.setName(Menu.DEFAULT_NAME);
            menuDao.saveOrUpdate(menu);

            mealTime.getMenus().add(menu);
            mealTimeDao.saveOrUpdate(mealTime);
        }
    }

    public void deleteMealTime(String dayName, String time) throws SQLException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Meal Time");
            } else {
                MealTime mealTime = optionalMealTime.get();
                day.getMealTimes().remove(mealTime);

                List<Menu> menuList = mealTime.getMenus();
                for (Menu menu : menuList) {
                    for (MenuItem menuItem : menu.getMenuItems()) {
                        menuItemDao.delete(menuItem);
                    }
                    menuDao.delete(menu);
                }

                dayDao.saveOrUpdate(day);
                mealTimeDao.delete(mealTime);
            }
        }
    }

    public void addBreakfastLunch() throws SQLException {
        List<Day> days = dayDao.findAll();

        for (Day day : days) {
            MealTime breakfast = new MealTime(day, "breakfast");
            MealTime lunch = new MealTime(day, "lunch");

            mealTimeDao.saveOrUpdate(breakfast);
            mealTimeDao.saveOrUpdate(lunch);

            Menu menu = new Menu();
            Menu menu1 = new Menu();

            menuDao.saveOrUpdate(menu);
            menuDao.saveOrUpdate(menu1);

            menu.setName(Menu.DEFAULT_NAME);
            menu.setMealTime(lunch);
            lunch.getMenus().add(menu);

            menu1.setName(Menu.DEFAULT_NAME);
            menu1.setMealTime(breakfast);
            breakfast.getMenus().add(menu1);

            menuDao.saveOrUpdate(menu);
            menuDao.saveOrUpdate(menu1);

            mealTimeDao.saveOrUpdate(breakfast);
            mealTimeDao.saveOrUpdate(lunch);


            day.getMealTimes().clear();
            day.getMealTimes().add(breakfast);
            day.getMealTimes().add(lunch);


            dayDao.saveOrUpdate(day);

        }
    }

    public void deleteMealTime(int id) throws SQLException {
        Optional<MealTime> optionalMealTime = mealTimeDao.find(id);
        if (optionalMealTime.isPresent()) {
            MealTime mealTime = optionalMealTime.get();
            mealTimeDao.delete(mealTime);
        }
    }
}
