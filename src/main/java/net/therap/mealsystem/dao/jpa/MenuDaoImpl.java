package net.therap.mealsystem.dao.jpa;

import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exception.MealTimeNotValidException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/15/20
 */
public class MenuDaoImpl implements MenuDao {

    private EntityManager em;
    private MealTimeDao mealTimeDao;

    public MenuDaoImpl(EntityManager em, MealTimeDao mealTimeDao) {
        this.em = em;
        this.mealTimeDao = mealTimeDao;
    }

    public Optional<Menu> find(int id) {
        Menu menu = em.find(Menu.class, id);

        return Optional.ofNullable(menu);
    }

    public Optional<Menu> find(Menu menu) {
        TypedQuery<Menu> query = em.createQuery("FROM Menu WHERE mealTime = :mealTime AND name = :name", Menu.class);
        query.setParameter("mealTime", menu.getMealTime());
        query.setParameter("name", menu.getName());

        try {
            Menu menu1 = query.getSingleResult();
            return Optional.of(menu1);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Menu> findFirst(MealTime mealTime) throws SQLException, MealTimeNotValidException {
        Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(mealTime.getDay(), mealTime.getTime());
        if (!optionalMealTime.isPresent()) {
            throw new MealTimeNotValidException();
        }

        mealTime = optionalMealTime.get();

        Query query = em.createQuery("FROM Menu WHERE mealTime = :mealTime");
        query.setParameter("mealTime", mealTime);

        Menu menu = (Menu) query.getSingleResult();

        return Optional.ofNullable(menu);
    }

    @Override
    public List<Menu> findAll() {
        TypedQuery<Menu> query = em.createQuery("FROM Menu ORDER BY id", Menu.class);

        return query.getResultList();
    }

    @Override
    public List<Menu> findByDay(Day day) throws SQLException {
        List<MealTime> mealTimes = mealTimeDao.findByDay(day);

        TypedQuery<Menu> query = em.createQuery("FROM Menu menu WHERE menu.mealTime IN (:mealTimes)", Menu.class);
        query.setParameter("mealTimes", mealTimes);

        return query.getResultList();
    }

    @Override
    public void saveOrUpdate(Menu menu) throws SQLException {
        Optional<Menu> optionalMenu = find(menu);

        if (optionalMenu.isPresent()) {
            update(menu);
        } else {
            save(menu);
        }
    }

    @Override
    public void save(Menu menu) {
        em.getTransaction().begin();

        em.persist(menu);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void update(Menu menu) {
        em.getTransaction().begin();

        em.merge(menu);

        em.flush();
        em.getTransaction().commit();
    }

    @Override
    public void delete(Menu menu) {
        em.getTransaction().begin();

        find(menu).ifPresent(menu1 -> em.remove(menu1));

        em.flush();
        em.getTransaction().commit();
    }
}
