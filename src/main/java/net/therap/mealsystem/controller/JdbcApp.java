package net.therap.mealsystem.controller;

import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.jdbc.JdbcFoodDao;
import net.therap.mealsystem.dao.jdbc.JdbcMealTimeDao;
import net.therap.mealsystem.dao.jdbc.JdbcMenuDao;
import net.therap.mealsystem.dao.jdbc.JdbcMenuItemDao;
import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.helper.MigrationHelper;
import net.therap.mealsystem.mapper.FoodMapper;
import net.therap.mealsystem.mapper.MealTimeMapper;
import net.therap.mealsystem.mapper.MenuItemMapper;
import net.therap.mealsystem.mapper.MenuMapper;
import net.therap.mealsystem.util.MySqlConnectionManager;
import net.therap.mealsystem.view.FoodViewer;
import net.therap.mealsystem.view.JdbcMenuViewer;
import net.therap.mealsystem.view.MenuViewer;
import net.therap.mealsystem.view.Viewer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
@Deprecated
public class JdbcApp extends App {

    private Connection connection;

    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;
    private JdbcMenuItemDao menuItemDao;
    private MenuDao menuDao;


    public JdbcApp() throws SQLException {
        this.connection = MySqlConnectionManager.createConnection();
        this.foodDao = new JdbcFoodDao(connection, new FoodMapper());
        this.mealTimeDao = new JdbcMealTimeDao(connection, new MealTimeMapper());
        this.menuItemDao = new JdbcMenuItemDao(connection, new MenuItemMapper(foodDao));
        this.menuDao = new JdbcMenuDao(connection, new MenuMapper(mealTimeDao, menuItemDao), mealTimeDao, menuItemDao);
    }

    @Override
    public void run(String[] args) throws SQLException {
        if (connection != null) {
            super.run(args);
        } else {
            System.err.println("Failed to make connection!");
        }
    }

    @Override
    public void refreshAllMigrations() throws SQLException {
        MigrationHelper.refreshAll(connection);
    }

    @Override
    public void rollbackAllMigrations() throws SQLException {
        MigrationHelper.rollbackAll(connection);
    }

    @Override
    public void migrateAllMigrations() throws SQLException {
        MigrationHelper.migrateAll(connection);
    }

    @Override
    public void shutdown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public void showMenu() throws SQLException {
        List<Menu> menuItems;
        menuItems = menuDao.findAll();

        MenuViewer viewer = new JdbcMenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems);
    }

    @Override
    public void showMenu(String dayName) throws SQLException {
        Day day = new Day(dayName);
        List<Menu> menuItems;
        menuItems = menuDao.findByDay(day);

        MenuViewer viewer = new JdbcMenuViewer(foodDao, mealTimeDao);
        viewer.view(menuItems, new Day[]{day});
    }

    @Override
    public void removeItem(String foodName, String dayName, String time) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.findByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
        } else {
            if (!mealTimeDao.findByDayTime(day, time).isPresent()) {
                System.err.println("There is no meal at that specified time");
            } else {
                MealTime mealTime = mealTimeDao.findByDayTime(day, time).get();
                Food food = foodDao.findByName(foodName).get();

                Optional<MenuItem> potentialMenuItem;
                potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
                if (potentialMenuItem.isPresent()) {
                    menuItemDao.delete(potentialMenuItem.get());
                } else {
                    System.err.println(foodName + " is not in the menu at the specified time");
                }
            }
        }
    }

    @Override
    public void updateItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.findByName(foodName).isPresent()) {
            System.err.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.findByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.saveOrUpdate(mealTime);
            }

            MealTime mealTime = mealTimeDao.findByDayTime(day, time).get();
            Food food = foodDao.findByName(foodName).get();

            Optional<MenuItem> potentialMenuItem;
            potentialMenuItem = menuItemDao.getMenuItemByData(mealTime.getId(), food.getId());
            if (potentialMenuItem.isPresent()) {
                MenuItem menuItem = potentialMenuItem.get();
                menuItem.setMaximumAmount(maxAmount);
                menuItemDao.update(mealTime, menuItem);
            } else {
                System.err.println("Item not in menu at specified time.");
            }
        }
    }

    @Override
    public void addItem(String foodName, String dayName, String time, String maxAmount) throws SQLException {
        Day day = new Day(dayName);
        if (!foodDao.findByName(foodName).isPresent()) {
            System.out.println(foodName + " is not in food list. It won't be added to menu.");
            //                            foodDao.save(new Food(foodName));
        } else {
            if (!mealTimeDao.findByDayTime(day, time).isPresent()) {
                MealTime mealTime = new MealTime(day, time);
                mealTimeDao.saveOrUpdate(mealTime);
            }

            MealTime mealTime = mealTimeDao.findByDayTime(day, time).get();
            Food food = foodDao.findByName(foodName).get();

            if (menuItemDao.getMenuItemByData(mealTime.getId(), food.getId()).isPresent()) {
                System.err.println("Item already in menu, not added");
            } else {
                MenuItem menuItem = new MenuItem(mealTime.getId(), food, maxAmount);
                menuItemDao.save(mealTime, menuItem);
            }
        }
    }

    @Override
    public void viewFoods() throws SQLException {
        List<Food> foods = foodDao.findAll();

        Viewer<Food> viewer = new FoodViewer();
        viewer.view(foods);
    }

    @Override
    public void deleteFood(String foodName) throws SQLException {
        Optional<Food> potentialFood = foodDao.findByName(foodName);
        if (potentialFood.isPresent()) {
            Food food = potentialFood.get();
            foodDao.delete(food);
        } else {
            System.err.println(foodName + " does not exist in list");
        }
    }

    @Override
    public void addFood(String foodName) throws SQLException {
        Food food = new Food(foodName);
        if (foodDao.findByName(foodName).isPresent()) {
            System.err.println(foodName + " already exists in food list");
        } else {
            foodDao.saveOrUpdate(food);
        }
    }
}
