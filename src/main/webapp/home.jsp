<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Therap MealSystems</title>
</head>
<body>
<center>
    <h1>
        <a href="${pageContext.request.contextPath}">Therap MealSystems</a>
    </h1>
    <a href="${pageContext.request.contextPath}/logout">Logout</a>
</center>

<center>
    <a href="${pageContext.request.contextPath}/food">Available Food List</a>
    |
    <a href="${pageContext.request.contextPath}/menu">Daily Meal Menu</a>
</center>

<link rel="stylesheet" type="text/css" href="lib/semantic-ui/semantic.min.css">
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="lib/semantic-ui/semantic.min.js"></script>
</body>
</html>
