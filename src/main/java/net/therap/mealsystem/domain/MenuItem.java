package net.therap.mealsystem.domain;

import javax.persistence.*;

/**
 * @author tanmoy.das
 * @since 3/8/20
 */
@Entity
@Table(name = "menu_items", uniqueConstraints = @UniqueConstraint(columnNames = {"menu_id", "food_id"}))
public class MenuItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "food_id")
    private Food food;

    @Column(name = "maximum_amount")
    private String maximumAmount;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    private Menu menu;

    public MenuItem() {
        this.maximumAmount = "unlimited";
    }

    public MenuItem(Food food, String maximumAmount) {
        this.food = food;
        this.maximumAmount = maximumAmount;
    }

    public MenuItem(int id, Food food, String maximumAmount) {
        this.id = id;
        this.food = food;
        this.maximumAmount = maximumAmount;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "food=" + food +
                ", maximumAmount='" + maximumAmount + '\'' +
                '}';
    }
}
