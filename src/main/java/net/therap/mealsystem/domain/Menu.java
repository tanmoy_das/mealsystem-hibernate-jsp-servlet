package net.therap.mealsystem.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
@Entity
@Table(name = "menus", uniqueConstraints = @UniqueConstraint(columnNames = {"name", "meal_time_id"}))
public class Menu {

    public static final String DEFAULT_NAME = "general";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "meal_time_id")
    private MealTime mealTime;

    private String name;

    @OneToMany(mappedBy = "menu", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MenuItem> menuItems;


    public Menu() {
        this.menuItems = new ArrayList<>();
        this.name = DEFAULT_NAME;
    }

    public Menu(MealTime mealTime, List<MenuItem> menuItems) {
        this();
        this.mealTime = mealTime;
        this.menuItems = menuItems;
    }

    public Menu(String name, MealTime mealTime, List<MenuItem> menuItems) {
        this();
        this.mealTime = mealTime;
        this.menuItems = menuItems;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public void setMealTime(MealTime mealTime) {
        this.mealTime = mealTime;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "name='" + name + '\'' +
                ", mealTime=" + mealTime +
                ", menuItems=" + menuItems +
                '}';
    }
}
