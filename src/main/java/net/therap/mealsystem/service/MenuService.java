package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.jpa.MenuItemDaoImpl;
import net.therap.mealsystem.domain.*;
import net.therap.mealsystem.exception.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
public class MenuService {

    private DayDao dayDao;
    private FoodDao foodDao;
    private MealTimeDao mealTimeDao;
    private MenuDao menuDao;
    private MenuItemDaoImpl menuItemDao;

    public MenuService(DayDao dayDao, FoodDao foodDao, MealTimeDao mealTimeDao, MenuDao menuDao, MenuItemDaoImpl menuItemDao) {
        this.dayDao = dayDao;
        this.foodDao = foodDao;
        this.mealTimeDao = mealTimeDao;
        this.menuDao = menuDao;
        this.menuItemDao = menuItemDao;
    }

    public void addMenu(String dayName, String time, String menuName) throws SQLException, DayNotValidException, MealTimeNotValidException, DuplicateEntryException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        addMenu(time, menuName, optionalDay);
    }

    public void addMenu(int dayId, String time, String menuName) throws SQLException, DayNotValidException, MealTimeNotValidException, DuplicateEntryException {
        Optional<Day> optionalDay = dayDao.find(dayId);

        addMenu(time, menuName, optionalDay);
    }

    private void addMenu(String time, String menuName, Optional<Day> optionalDay) throws SQLException, DayNotValidException, MealTimeNotValidException, DuplicateEntryException {
        if (!optionalDay.isPresent()) {
            throw new DayNotValidException();
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                throw new MealTimeNotValidException();
            } else {
                MealTime mealTime = optionalMealTime.get();

                Menu menu = new Menu();
                menu.setName(menuName);
                menu.setMealTime(mealTime);

                Optional<Menu> existingMenu = menuDao.find(menu);

                if (existingMenu.isPresent()) {
                    throw new DuplicateEntryException();
                }

                mealTime.getMenus().add(menu);

                menuDao.saveOrUpdate(menu);
                mealTimeDao.saveOrUpdate(mealTime);
            }
        }
    }

    public void deleteMenu(String dayName, String time, String menuName) throws SQLException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);

        if (!optionalDay.isPresent()) {
            System.err.println("Invalid Day Name");
        } else {
            Day day = optionalDay.get();
            Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(day, time);

            if (!optionalMealTime.isPresent()) {
                System.err.println("Invalid Meal Time");
            } else {
                MealTime mealTime = optionalMealTime.get();

                Menu menu = menuDao.findAll()
                        .stream()
                        .filter(listedMenu -> mealTime.toString().equals(listedMenu.getMealTime().toString())
                                && menuName.equals(listedMenu.getName()))
                        .findFirst()
                        .orElse(null);

                if (menu != null) {
                    mealTime.getMenus().remove(menu);

                    for (MenuItem menuItem : menu.getMenuItems()) {
                        menuItemDao.delete(menuItem);
                    }
                    menu.getMenuItems().clear();

                    mealTimeDao.saveOrUpdate(mealTime);
                    menuDao.delete(menu);
                }
            }
        }
    }

    public List<Menu> getAllMenus() throws SQLException {
        List<Menu> menus = menuDao.findAll();
        return menus;
    }

    public List<Menu> getMenusByDayName(String dayName) throws SQLException, DayNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);
        if (!optionalDay.isPresent()) {
            throw new DayNotValidException();
        } else {
            Day day = optionalDay.get();
            return menuDao.findByDay(day);
        }
    }

    public Optional<Menu> findMenu(String dayName, String time, String menuName)
            throws SQLException, DayNotValidException, MealTimeNotValidException {
        Optional<Day> optionalDay = dayDao.findByName(dayName);
        if (!optionalDay.isPresent()) {
            throw new DayNotValidException();
        }
        Day day = optionalDay.get();

        Optional<MealTime> optionalMealTime = mealTimeDao.findByDayTime(day, time);
        if (!optionalMealTime.isPresent()) {
            throw new MealTimeNotValidException();
        }
        MealTime mealTime = optionalMealTime.get();

        List<Menu> menus = mealTime.getMenus();

        for (Menu menu : menus) {
            if (menu.getName().equals(menuName)) {
                return Optional.of(menu);
            }
        }

        return Optional.empty();
    }

    public void removeItem(String foodName, String dayName, String time, String menuName)
            throws SQLException, MenuNotValidException, MealTimeNotValidException, DayNotValidException {
        Optional<Menu> optionalMenu = findMenu(dayName, time, menuName);

        if (!optionalMenu.isPresent()) {
            throw new MenuNotValidException();
        }

        Menu menu = optionalMenu.get();

        MenuItem menuItemToDelete = null;
        for (MenuItem menuItem : menu.getMenuItems()) {
            if (menuItem.getFood().getName().equals(foodName)) {
                menuItemToDelete = menuItem;
            }
        }

        if (menuItemToDelete == null) {
            System.err.println("Item not in the menu at specified time");
        } else {
            menu.getMenuItems().remove(menuItemToDelete);
            menuDao.saveOrUpdate(menu);
            menuItemDao.delete(menuItemToDelete);
        }

    }

    public void addItem(String foodName, String dayName, String time, String maxAmount, String mealName)
            throws SQLException, DayNotValidException, MealTimeNotValidException, MenuNotValidException {
        Optional<Menu> optionalMenu = findMenu(dayName, time, mealName);

        if (!optionalMenu.isPresent()) {
            throw new MenuNotValidException();
        } else {
            Menu menu = optionalMenu.get();

            Optional<Food> optionalFood = foodDao.findByName(foodName);
            if (!optionalFood.isPresent()) {
                System.err.println("Invalid Food name");
            } else {
                Food food = optionalFood.get();

                for (MenuItem menuItem : menu.getMenuItems()) {
                    if (menuItem.getFood().getName().equals(food.getName())) {
                        System.err.println("Food already in menu. " +
                                "To update the maximum amount use --updateItem option");
                        return;
                    }
                }

                MenuItem menuItem = new MenuItem(food, maxAmount);
                menu.getMenuItems().add(menuItem);
                menuItem.setMenu(menu);

                menuItemDao.save(menuItem);
                menuDao.saveOrUpdate(menu);
            }
        }
    }

    public void updateItem(String foodName, String dayName, String time, String maxAmount, String mealName)
            throws MealTimeNotValidException, SQLException, DayNotValidException, MenuNotValidException, MenuItemNotValidException {
        Optional<Menu> optionalMenu = findMenu(dayName, time, mealName);

        if (!optionalMenu.isPresent()) {
            throw new MenuNotValidException();
        } else {
            Menu menu = optionalMenu.get();

            MenuItem menuItemToUpdate = menu.getMenuItems().stream()
                    .filter(menuItem -> menuItem.getFood().getName().equals(foodName))
                    .findAny()
                    .orElseThrow(MenuItemNotValidException::new);

            menuItemToUpdate.setMaximumAmount(maxAmount);
            menuItemDao.update(menuItemToUpdate);
        }
    }

    public void deleteMenu(int id) throws SQLException {
        Optional<Menu> optionalMenu = menuDao.find(id);
        if (optionalMenu.isPresent()) {
            Menu menu = optionalMenu.get();

            MealTime mealTime = menu.getMealTime();

            mealTime.getMenus().remove(menu);
            mealTimeDao.saveOrUpdate(mealTime);

            menuDao.delete(menu);
        }
    }

    public void addMenuItem(int menuId, int foodId, String maxAmount) throws SQLException, FoodNotValidException, MenuNotValidException {
        Optional<Menu> optionalMenu = menuDao.find(menuId);
        Optional<Food> optionalFood = foodDao.find(foodId);

        if (optionalMenu.isPresent()) {
            if (optionalFood.isPresent()) {

                Menu menu = optionalMenu.get();
                Food food = optionalFood.get();

                MenuItem menuItem = new MenuItem();
                menuItemDao.save(menuItem);

                menuItem.setFood(food);
                menuItem.setMaximumAmount(maxAmount);
                menuItem.setMenu(menu);

                menuItemDao.update(menuItem);

                menu.getMenuItems().add(menuItem);
                food.getMenuItems().add(menuItem);

                menuDao.saveOrUpdate(menu);
                foodDao.saveOrUpdate(food);

            } else {
                throw new FoodNotValidException();
            }
        } else {
            throw new MenuNotValidException();
        }
    }

    public void deleteMenuItem(int menuItemId) throws SQLException, MenuItemNotValidException {
        Optional<MenuItem> optionalMenuItem = menuItemDao.find(menuItemId);

        if (optionalMenuItem.isPresent()) {
            MenuItem menuItem = optionalMenuItem.get();

            Food food = menuItem.getFood();
            food.getMenuItems().remove(menuItem);
            foodDao.saveOrUpdate(food);

            Menu menu = menuItem.getMenu();
            menu.getMenuItems().remove(menuItem);
            menuDao.saveOrUpdate(menu);

            menuItemDao.delete(menuItem);
        } else {
            throw new MenuItemNotValidException();
        }
    }

    public void updateItem(int menuItemId, String maxAmount) throws MenuItemNotValidException {
        Optional<MenuItem> optionalMenuItem = menuItemDao.find(menuItemId);

        if (optionalMenuItem.isPresent()) {
            MenuItem menuItemToUpdate = optionalMenuItem.get();

            menuItemToUpdate.setMaximumAmount(maxAmount);
            menuItemDao.update(menuItemToUpdate);
        } else {
            throw new MenuItemNotValidException();
        }
    }
}
