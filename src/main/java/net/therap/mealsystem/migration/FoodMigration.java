package net.therap.mealsystem.migration;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author tanmoy.das
 * @since 3/9/20
 */
public class FoodMigration implements Migratable {

    @Override
    public void migrate(Connection connection) throws SQLException {
        String sqlCreate = "CREATE TABLE IF NOT EXISTS foods"
                + "("
                + " id INT NOT NULL AUTO_INCREMENT,"
                + " name varchar(100) NOT NULL,"
                + " UNIQUE (`name`),"
                + " PRIMARY KEY (id)"
                + ")";

        connection.prepareStatement(sqlCreate).execute();
    }

    @Override
    public void rollback(Connection connection) throws SQLException {
        String sqlDrop = "DROP TABLE foods";
        connection.prepareStatement(sqlDrop).execute();
    }


}
