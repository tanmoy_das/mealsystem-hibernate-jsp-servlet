package net.therap.mealsystem.mapper;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/10/20
 */
public class MealTimeMapper {

    public Optional<MealTime> map(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            int id = resultSet.getInt("id");
            String dayName = resultSet.getString("day");
            String time = resultSet.getString("time");

            Day day = new Day(dayName);

            return Optional.of(new MealTime(id, day, time));
        } else {
            return Optional.empty();
        }
    }
}
