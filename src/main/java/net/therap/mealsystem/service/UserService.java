package net.therap.mealsystem.service;

import net.therap.mealsystem.dao.UserDao;
import net.therap.mealsystem.domain.Token;
import net.therap.mealsystem.domain.User;
import net.therap.mealsystem.util.RandomStringGenerator;

import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
public class UserService {

    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public Optional<User> getUserFromToken(String token) {
        return userDao.findUserByToken(token);
    }

    public void registerUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        userDao.saveOrUpdate(user);
    }

    public Optional<Token> loginUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        Optional<User> optionalUser = userDao.find(user);

        if (optionalUser.isPresent()) {
            user = optionalUser.get();

            RandomStringGenerator randomStringGenerator = new RandomStringGenerator();
            String tokenTxt = randomStringGenerator.getAlphaNumericString();

            Token token = new Token(tokenTxt);
            token.setUser(user);

            userDao.addToken(user, token);

            return Optional.of(token);
        }

        return Optional.empty();
    }

    public void invalidateToken(String tokenTxt) {
        Optional<User> optionalUser = getUserFromToken(tokenTxt);
        if (!optionalUser.isPresent()) {
            return;
        }

        User user = optionalUser.get();
        Optional<Token> optionalToken = user.getTokens()
                .stream()
                .filter(t -> t.getValue().equals(tokenTxt))
                .findAny();

        if (optionalToken.isPresent()) {
            Token token = optionalToken.get();
            user.getTokens().remove(token);

            userDao.deleteToken(user, token);
        }
    }
}
