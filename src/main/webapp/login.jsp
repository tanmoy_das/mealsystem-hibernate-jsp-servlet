<%--
  @author tanmoy.das
  @since 3/24/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Therap MealSystems</title>
</head>
<body>
<center>
    <h1>Login Form</h1>
    <form action="${pageContext.request.contextPath}/login" method="post">
        <label>
            Email:
            <input type="email" name="email">
        </label>
        <label>
            Password:
            <input type="password" name="password">
        </label>

        <input type="submit">
    </form>

    <div>Don't have an acount? <a href="${pageContext.request.contextPath}/register">Register Here</a></div>
</center>

<link rel="stylesheet" type="text/css" href="lib/semantic-ui/semantic.min.css">
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="lib/semantic-ui/semantic.min.js"></script>
</body>
</html>
