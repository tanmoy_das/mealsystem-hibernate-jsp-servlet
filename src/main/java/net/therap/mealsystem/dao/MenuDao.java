package net.therap.mealsystem.dao;

import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.domain.Menu;
import net.therap.mealsystem.exception.MealTimeNotValidException;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * @author tanmoy.das
 * @since 3/12/20
 */
public interface MenuDao {
    Optional<Menu> findFirst(MealTime mealTime) throws SQLException, MealTimeNotValidException;

    List<Menu> findAll() throws SQLException;

    List<Menu> findByDay(Day day) throws SQLException;

    void save(Menu menu) throws SQLException;

    void update(Menu menu) throws SQLException;

    void saveOrUpdate(Menu menu) throws SQLException;

    void delete(Menu menu) throws SQLException;

    Optional<Menu> find(int id) throws SQLException;

    Optional<Menu> find(Menu menu) throws SQLException;
}
