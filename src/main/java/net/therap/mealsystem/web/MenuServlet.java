package net.therap.mealsystem.web;

import net.therap.mealsystem.dao.DayDao;
import net.therap.mealsystem.dao.FoodDao;
import net.therap.mealsystem.dao.MealTimeDao;
import net.therap.mealsystem.dao.MenuDao;
import net.therap.mealsystem.dao.jpa.*;
import net.therap.mealsystem.domain.Day;
import net.therap.mealsystem.domain.Food;
import net.therap.mealsystem.domain.MealTime;
import net.therap.mealsystem.exception.*;
import net.therap.mealsystem.service.DayService;
import net.therap.mealsystem.service.FoodService;
import net.therap.mealsystem.service.MealTimeService;
import net.therap.mealsystem.service.MenuService;
import net.therap.mealsystem.util.HibernateUtil;
import net.therap.mealsystem.web.middleware.AuthMiddleware;
import org.hibernate.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/23/20
 */
@WebServlet(
        name = "MenuServlet",
        urlPatterns = {
                "/menu",
                "/addMealTime",
                "/addMenu",
                "/deleteMealTime",
                "/deleteMenu",
                "/addMenuItem",
                "/deleteMenuItem",
                "/updateMenuItem"
        }
)
public class MenuServlet extends HttpServlet {

    private FoodService foodService;
    private DayService dayService;
    private MealTimeService mealTimeService;
    private MenuService menuService;

    public MenuServlet() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        DayDao dayDao = new DayDaoImpl(session);
        this.dayService = new DayService(dayDao);

        FoodDao foodDao = new FoodDaoImpl(session);
        this.foodService = new FoodService(foodDao);

        MealTimeDao mealTimeDao = new MealTimeDaoImpl(session);

        MenuDao menuDao = new MenuDaoImpl(session, mealTimeDao);
        MenuItemDaoImpl menuItemDao = new MenuItemDaoImpl(session);

        this.mealTimeService = new MealTimeService(dayDao, mealTimeDao, menuDao, menuItemDao);
        this.menuService = new MenuService(dayDao, foodDao, mealTimeDao, menuDao, menuItemDao);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AuthMiddleware authMiddleware = new AuthMiddleware();

        if (authMiddleware.isGuest(req, resp)) {
            resp.sendRedirect(req.getContextPath());
        } else {
            super.service(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Day> days = dayService.getAllDays();
            List<Food> foods = foodService.getAllFoods();

            for (Day day : days) {
                day.getMealTimes().sort(Comparator.comparingInt(MealTime::getId));
            }

            req.setAttribute("days", days);
            req.setAttribute("foods", foods);


            RequestDispatcher requestDispatcher = req.getRequestDispatcher("menu.jsp");
            requestDispatcher.forward(req, resp);

        } catch (SQLException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("_method");

        String addMealTimePath = req.getContextPath() + "/addMealTime";
        String addMenuPath = req.getContextPath() + "/addMenu";
        String addMenuItemPath = req.getContextPath() + "/addMenuItem";

        try {

            if (method == null) {

                if (req.getRequestURI().equals(addMealTimePath)) {
                    handleAddMealTime(req, resp);
                } else if (req.getRequestURI().equals(addMenuPath)) {
                    handleAddMenu(req, resp);
                } else if (req.getRequestURI().equals(addMenuItemPath)) {
                    handleAddMenuItem(req, resp);
                }

                resp.sendRedirect(req.getContextPath() + "/menu");
            } else if (method.toUpperCase().equals("PUT")) {
                doPut(req, resp);
            } else if (method.toUpperCase().equals("DELETE")) {
                doDelete(req, resp);
            }

        } catch (SQLException | DayNotValidException | FoodNotValidException | MenuNotValidException |
                MealTimeNotValidException | DuplicateEntryException e) {

            resp.sendError(500, e.getMessage());
            e.printStackTrace();

        }
    }

    private void handleAddMenuItem(HttpServletRequest req, HttpServletResponse resp) throws SQLException, FoodNotValidException, MenuNotValidException {
        int menuId = Integer.parseInt(req.getParameter("menu_id"));
        int foodId = Integer.parseInt(req.getParameter("food_id"));
        String maxAmount = req.getParameter("maximum_amount");

        menuService.addMenuItem(menuId, foodId, maxAmount);
    }

    private void handleAddMenu(HttpServletRequest req, HttpServletResponse resp) throws SQLException, DayNotValidException, MealTimeNotValidException, DuplicateEntryException {
        int dayId = Integer.parseInt(req.getParameter("day_id"));
        String time = req.getParameter("time");
        String name = req.getParameter("name");

        menuService.addMenu(dayId, time, name);
    }

    private void handleAddMealTime(HttpServletRequest req, HttpServletResponse resp) throws SQLException, DayNotValidException {
        int id = Integer.parseInt(req.getParameter("day_id"));
        String time = req.getParameter("time");
        mealTimeService.addMealTime(id, time);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String updateMenuItemPath = req.getContextPath() + "/updateMenuItem";

        try {
            if (req.getRequestURI().equals(updateMenuItemPath)) {
                handleUpdateMenuItem(req, resp);
            }

            resp.sendRedirect(req.getContextPath() + "/menu");
        } catch (MenuItemNotValidException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }
    }

    private void handleUpdateMenuItem(HttpServletRequest req, HttpServletResponse resp) throws MenuItemNotValidException {
        int menuItemId = Integer.parseInt(req.getParameter("menu_item_id"));
        String maxAmount = req.getParameter("maximum_amount");

        menuService.updateItem(menuItemId, maxAmount);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String deleteMealTimePath = req.getContextPath() + "/deleteMealTime";
        String deleteMenuPath = req.getContextPath() + "/deleteMenu";
        String deleteMenuItemPath = req.getContextPath() + "/deleteMenuItem";

        try {
            if (req.getRequestURI().equals(deleteMealTimePath)) {
                handleDeleteMealTime(req, resp);
            } else if (req.getRequestURI().equals(deleteMenuPath)) {
                handleDeleteMenu(req, resp);
            } else if (req.getRequestURI().equals(deleteMenuItemPath)) {
                handleDeleteMenuItem(req, resp);
            }

            resp.sendRedirect(req.getContextPath() + "/menu");
        } catch (SQLException | MenuItemNotValidException e) {
            resp.sendError(500, e.getMessage());
            e.printStackTrace();
        }

    }

    private void handleDeleteMenuItem(HttpServletRequest req, HttpServletResponse resp) throws SQLException, MenuItemNotValidException {
        int menuItemId = Integer.parseInt(req.getParameter("menu_item_id"));

        menuService.deleteMenuItem(menuItemId);
        System.out.println(1 + "\n\n" + "tkd 1");
    }

    private void handleDeleteMenu(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        int id = Integer.parseInt(req.getParameter("menu_id"));
        menuService.deleteMenu(id);
    }

    private void handleDeleteMealTime(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        int id = Integer.parseInt(req.getParameter("meal_time_id"));
        mealTimeService.deleteMealTime(id);
    }
}
