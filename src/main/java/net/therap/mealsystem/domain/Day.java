package net.therap.mealsystem.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 3/11/20
 */
@Entity
@Table(name = "days", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
public class Day {

    public static final String[] DAY_NAMES = new String[]{"saturday", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday"};

    @Id
    private int id;

    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "day", cascade = CascadeType.ALL)
    private List<MealTime> mealTimes;

    public Day() {
        this.mealTimes = new ArrayList<>();
    }

    public Day(String name) {
        this();
        this.name = name;
    }

    public Day(int id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public List<MealTime> getMealTimes() {
        return mealTimes;
    }

    public void setMealTimes(List<MealTime> mealTimes) {
        this.mealTimes = mealTimes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
